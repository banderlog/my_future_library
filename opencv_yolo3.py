import cv2
import numpy as np
from typing import Tuple


class YOLO3:
    """ Base class for YOLO3 architecture inference and decode using OpenCV

        :param cfg_path: path to YOLO3 config file
        :param weights_path: path to YOLO3 weight file
        :param classes: model's classes, defaults to ``None``

        **Example:**

        .. code-block:: python

            from pkg_resources import resource_filename


            class RateMe(YOLO3):
                def __init__(self):
                    cfg_path = resource_filename(__name__, "yolov3-tiny_likedislike.cfg")
                    weights_path = resource_filename(__name__, "yolov3-tiny_likedislike_4000.weights")
                    YOLO3.__init__(self, cfg_path, weights_path, {0: 'like', 1: 'dislike'})
    """
    def __init__(self, cfg_path: str, weights_path: str,
                 classes: dict = None):
        """
            :param cfg_path: path to YOLO3 config file
            :param weights_path: path to YOLO3 weight file
            :param classes: model's classes, defaults to ``None``
        """
        # load net
        self.net = cv2.dnn.readNetFromDarknet(cfg_path, weights_path)
        # get proper output layers
        ln = self.net.getLayerNames()
        self.ln = [ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
        # define classes
        self.classes = classes

    def _infere(self, img: 'np.ndarray') -> 'Tuple[np.ndarray, int, int]':
        """ Preprocess and infere input image
        """
        # preprocess input image
        blob = cv2.dnn.blobFromImage(img, 1 / 255.0, (416, 416),
                                     swapRB=True, crop=False)
        self.net.setInput(blob)
        # infere
        layerOutputs = self.net.forward(self.ln)
        (H, W) = img.shape[:2]
        return layerOutputs, H, W

    def process(self, img: 'np.ndarray',
                conf: float = 0.5) -> 'Tuple[np.ndarray, np.ndarray, np.ndarray]':
        """ Preprocess, infere and decode results

            :param img: RGB image
            :param conf: confidence threshold, defaults to ``0.5``

            :return: bounding_boxes, confidences, class_IDs

            .. note::
                bounding_boxes format: [xc, yc, w, h]
        """
        boxes = []
        confidences = []
        classIDs = []
        layerOutputs, H, W = self._infere(img)

        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                # (centerX, centerY, width, height)
                box = detection[0:4] * np.array([W, H, W, H])
                boxes.append(box)
                confidences.append(float(confidence))
                classIDs.append(classID)
        # non maximum suppression
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, conf, 0.3)
        if len(idxs) == 0:
            return None, None, None
        boxes = np.take(np.array(boxes), idxs.flatten(), axis=0)
        confidences = np.take(np.array(confidences), idxs.flatten(), axis=0)
        classIDs = np.take(np.array(classIDs), idxs.flatten(), axis=0)
        return boxes, confidences, classIDs

    @staticmethod
    def get_points_check_borders(box: 'np.ndarray',
                                 img_shape: tuple) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        """
            Convert bounding boxes from YOLO format to OpenCV

            Also check bbox boundaries to image shape

            :param box: ``[xc, yc, w, h]``
            :return: ``(xmin, ymin), (xmax, ymax)``
        """
        H, W = img_shape
        xc, yc, w, h = box
        xmin = int(xc - w / 2)
        if xmin < 0:
            xmin = 0
        ymin = int(yc - h / 2)
        if ymin < 0:
            ymin = 0
        xmax = int(xc + w / 2)
        if xmax > W:
            xmax = W
        ymax = int(yc + h / 2)
        if ymax > H:
            ymax = H
        return (xmin, ymin), (xmax, ymax)

    def plot_detections(self, img: 'np.ndarray', boxes: 'np.ndarray',
                        confidences: 'np.ndarray', labels: 'np.ndarray',
                        thickness: int = 2, font_size: float = 0.5) -> 'np.ndarray':
        """ Plot YOLO3 detections on frame

            :param img: RGB image
            :param boxes: bounding_boxes from :class:`process()`
            :param confidences: confidences from :class:`process()`
            :param labels: labels from :class:`process()`
            :param thickness: bounding box thickness, defaults to ``2``
            :param font_size: labels font size, defaults to ``0.5``
            :return: RGB image
        """
        tmp = img.copy()
        if boxes is not None:
            for b, c, l in zip(boxes, confidences, labels):
                p1, p2 = self.get_points_check_borders(b, img.shape[:2])
                cv2.rectangle(tmp, p1, p2, (0, 255, 0), thickness)
                if self.classes:
                    cl_name = self.classes[l]
                else:
                    cl_name = l
                text = "{}: {}".format(cl_name, str(round(c, 2)))
                cv2.putText(tmp, text, p1, cv2.FONT_HERSHEY_SIMPLEX,
                            font_size, (255, 0, 0), thickness)
        return tmp

    @staticmethod
    def get_blob(img: 'np.ndarray', bbox: 'np.ndarray',
                 out_shape=(128, 128)) -> 'np.ndarray':
        """
            Preprocess image for inference in OpenCV's `dldt` module

            :param img: RGB image
            :param bbox: YOLO bounding box ``[xc, yc, w, h]``
            :param out_shape: shape of output, defaults to ``(128, 128)``
            :return blob: resized image with additional axis (blob size one)
        """
        blob = None
        p1, p2 = YOLO3.get_points_check_borders(bbox, img.shape[:2])
        xmin, ymin = p1
        xmax, ymax = p2
        patch = img[ymin:ymax, xmin:xmax]
        blob = cv2.resize(patch, out_shape)
        return blob
