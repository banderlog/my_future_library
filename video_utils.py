import cv2
import magic
import os
from tqdm import tqdm
import numpy as np
import functools
from typing import Callable


class CAM():
    """ Create video capture object with smallest buffer possible
    """
    def __init__(self, camid: int):
        """:param caimid: WebCam id in system
        """
        self.video = cv2.VideoCapture(camid)
        self.video.set(cv2.CAP_PROP_BUFFERSIZE, 1)

    def next(self) -> 'np.ndarray':
        """ Return new frame each time
        """
        # drop one ine buffer
        self.video.grab()
        self.video.grab()
        success, frame = self.video.retrieve()
        return frame


def get_single_frame(img_path: str) -> 'np.ndarray':
    """ Connect to video stream and grab a frame

        :param img_path: full path to video stream
    """
    cam = cv2.VideoCapture(img_path)
    success, frame = cam.read()
    cam.release()
    return frame


def check_img_path(img_path: str) -> bool:
    """ Check videostream existance

        :param img_path: full path to video stream
    """
    answer = False
    if img_path.startswith("rtsp://"):
        answer = True
    elif os.path.isfile(img_path):
        if ('video' or 'image') in magic.from_file(img_path, mime=True):
            answer = True
    return answer


def videowriter_wrapper(func: 'Callable', IN: str, OUT: str, CODEC: str = "MP4V",
                        out_shape: tuple = None, time: int = None,
                        from_frame: int = None, *args, **kwargs) -> 'Callable':
    """ :param func: function in form ``func(frame, *args, **kwargs) -> frame``
        :param IN: file or rtsp stream
        :param OUT: file name
        :param CODEC: ``"MP4V"`` or ``"MJPG"`` or (``"H264"`` not for opencv-python), defaults to ``"MP4V"``
        :param out_shape: ``tuple(width, height)``, defaults to ``None``
        :param time: seconds to crop from start, defaults to ``None``
        :param from_frame: start frame position, defaults to ``None``

        **Examples:**

        .. code-block:: python

            write_15s = videowriter_wrapper(lambda x: x, path_img, out_path, time=15)
            write_15s()

            process_video = videowriter_wrapper(plot_persons, args.IN, args.OUT)
            process_video(yolo, model, shape=(224, 224))
    """
    @functools.wraps(func)
    def inner(*args, **kwargs):
        # open input file
        INPUT = cv2.VideoCapture(IN)
        if out_shape:
            W, H = out_shape
        else:
            W = int(INPUT.get(cv2.CAP_PROP_FRAME_WIDTH))
            H = int(INPUT.get(cv2.CAP_PROP_FRAME_HEIGHT))
        FPS = float(INPUT.get(cv2.CAP_PROP_FPS))
        if time:
            TARGET_FRAMES = int(FPS * time)
            if from_frame:
                delta = int(from_frame)
                INPUT.set(cv2.CAP_PROP_POS_FRAMES, delta)
        else:
            TARGET_FRAMES = int(INPUT.get(cv2.CAP_PROP_FRAME_COUNT))
            if from_frame:
                delta = int(from_frame)
                INPUT.set(cv2.CAP_PROP_POS_FRAMES, delta)
                TARGET_FRAMES -= delta

        # open output file
        fourcc = cv2.VideoWriter_fourcc(*CODEC)
        writer = cv2.VideoWriter(OUT, fourcc, FPS, (W, H), True)

        # process every frame
        for i in tqdm(range(TARGET_FRAMES)):
            if INPUT.isOpened():
                grabbed, frame = INPUT.read()
                ###
                frame = func(frame, *args, **kwargs)
                ###
                if out_shape:
                    frame = cv2.resize(frame, out_shape)
                writer.write(frame)
        # close files
        writer.release()
        INPUT.release()
    return inner
