""" From https://github.com/keras-team/keras/issues/6507#issuecomment-322857357
"""
import numpy as np
from tensorflow.keras import backend as K


def precision(y_true: 'np.ndarray', y_pred: 'np.ndarray') -> float:
    " Calculates the precision "
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true: 'np.ndarray', y_pred: 'np.ndarray') -> float:
    " Calculates the recall "
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def fbeta_score(y_true: 'np.ndarray', y_pred: 'np.ndarray',
                beta: int = 1) -> float:
    " Calculates the f-measure, the harmonic mean of precision and recall "
    if beta < 0:
        raise ValueError('The lowest choosable beta is zero \
                          (only precision).')

    # If there are no true positives,
    # fix the F score at 0 like sklearn.
    if K.sum(K.round(K.clip(y_true, 0, 1))) == 0:
        return 0

    p = precision(y_true, y_pred)
    r = recall(y_true, y_pred)
    bb = beta ** 2
    fbeta_score = (1 + bb) * (p * r) / (bb * p + r + K.epsilon())
    return fbeta_score


def fmeasure(y_true: 'np.array', y_pred: 'np.array') -> float:
    " Wrapper for ``fbeta_score()`` "
    return fbeta_score(y_true, y_pred, beta=1)
