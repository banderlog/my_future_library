import numpy as np
from typing import Tuple


def get_random_image(shape: Tuple[int, int, int]) -> 'np.ndarray':
    """ Generate image of random noise

        :param shape: ``(height, width, channels_num)``
    """
    img = (np.random.standard_normal(shape) * 255).astype(np.uint8)
    return img
