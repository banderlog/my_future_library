import pandas as pd
import numpy as np

from typing import Tuple

from training_utils.categories import reduce_secondary_plan_type
from training_utils.categories import reduce_cob_ed_priority


def prepare_one_hot(df_cat: pd.DataFrame, *, col_to_convert: list) -> pd.DataFrame:
    """ Encode provided feature list to one-hotted features

        :param df_cat: input dataset
        :param col_to_convert: list of features to convert
        :return: df
    """
    # convert and collect to list
    tmp = []
    for i in col_to_convert:
        tmp.append(pd.get_dummies(df_cat[i], prefix=i, dummy_na=True))

    # merge with original, substitute originals
    df_onehot = pd.concat(tmp, axis=1)
    df = pd.concat([df_cat.drop(columns=col_to_convert), df_onehot], axis=1)

    # df = df.astype('uint8')
    return df


def relabel_data(df_all: pd.DataFrame) -> pd.DataFrame:
    """ Relable data, using all rows for all patients  even if they have only 1 row for 90d prediction.
        Patients with >=6 rows were used more than once.

        :param df_all: input df
        :return: df
    """
    # create sorted multiindex
    df_all = df_all.set_index(['mpi', 'report_month']).sort_index()

    # keep old labeling as boolean
    df_all['old_label'] = df_all['record_type'].copy().map({'CASE': True, 'CONTROL': False})
    # by default all guys are controls
    df_all['record_type'] = 'CONTROL'

    tmp = []
    for mpi, group in df_all[['record_type', 'old_label']].groupby('mpi'):
        # select each third -- 90d prediction
        group = group.iloc[::3, ::].copy()
        # last record is the case, if patient did not made old_label threshold
        if group['old_label'].iat[-1]:
            group.loc[:, 'record_type'].iat[-1] = 'CASE'
        tmp.append(group)
    # merge to df
    df_label = pd.concat(tmp).sort_index()

    # select by index
    df_sel = df_all.loc[df_label.index]
    # set new target feature
    df_sel['record_type'] = df_label['record_type']
    # keep old target feature
    df_sel['old_label'] = df_label['old_label']

    return df_sel


def postprocess(df_: pd.DataFrame) -> pd.DataFrame:
    """ Various postprocessing

        :param df_: input df
        :return: df
    """
    df = df_.copy()

    # fill na
    df['first_months_flag'].fillna('None', inplace=True)
    df['cob_ed_priority'] = df['cob_ed_priority'].fillna('NA')
    df['imt_loss'] = df['imt_loss'].fillna(0)
    # df['cob_ed_priority'] = df['cob_ed_priority'].fillna('None')

    # map target
    df['record_type'] = df['record_type'].map({'CASE': 1, 'CONTROL': 0})

    # report_month is index
    dates = [
        'cob_date', 'fdod_dva', 'fdod_ever', 'loss_month',
        'report_date', 'patient_dob', 'turn_65_date',
        'cob_end_date',
        'fdod_dva_oracle', 'fdod_ever_oracle'
    ]
    # convert dates to days, days to intervals
    for d in dates:
        df[d] = (df[d] - df['scoringdate']).dt.days
        df[d] = pd.cut(df[d], 10)

    # special case, days only, no intervals
    d = 'last_cob'
    df[d] = (df[d] - df['scoringdate']).dt.days

    # reduce last_oe to binary variable
    d = 'last_oe'
    df[d] = (df[d] - df['scoringdate']).dt.days
    mask = df[d] < 100
    df.loc[mask, d] = '<100days'
    df.loc[~mask, d] = '>=100days'

    # change dtype
    for c in df.columns.difference(['record_type', 'last_cob']):
        df[c] = df[c].astype('category')

    # drop this column
    df = df.drop(columns=['scoringdate'])

    # reduce categories
    df = reduce_secondary_plan_type(df)
    df = reduce_cob_ed_priority(df)

    return df


def encode_categorical(df_: pd.DataFrame) -> Tuple[pd.DataFrame, dict]:
    """ Replace categorical feature values with their codes

        :param df_: dataframe
        :return: updated dataframe and categorical feature dictionary
    """
    df = df_.copy()

    # create cat_dict
    cat_dict = dict()
    for c in df.select_dtypes(include='category').columns:
        cat_dict.update({c: df[c].cat.categories.tolist()})

    # set values to cat codes
    for c in df.select_dtypes(include='category').columns:
        # category codes instead of raw values
        df[c] = df[c].cat.codes
        # nan always encoded to -1. We want nan back
        mask = df[c] == -1
        df.loc[mask, c] = np.nan

    return df, cat_dict


def filter_mpi_by_row_count(df, *, at_least_rows, missed_th=62) -> pd.DataFrame:
    """ Drop patients with < at_least_rows and gap between rows > 62
        Normally, gap should be ~1 month, but we can tolerate more.

        :param df: input dataframe
        :param at_least_rows: if mpi has < at_least_rows we will drop it
        :param missed_th: maximal gap between report_dates per patient, days
        :return: df
    """
    # 1. DROP PATIENTS WITH < at_least_rows
    if at_least_rows > 1:
        tmp = df.groupby('mpi')['report_month'].count().sort_values()
        valid_mpi_by_row_count = tmp[tmp >= at_least_rows].index  # noqa: F841
        df = df.query('mpi in @valid_mpi_by_row_count')
        # keep only last `at_least_rows` rows before loss
        df = df.sort_values(['mpi', 'report_month'])
        df = df.groupby('mpi').tail(at_least_rows)

        # 2. DROP ALL WHO VIOLATED MISSED_THRESHOLD -- 1 month by default
        tmp = df.groupby('mpi')['report_month'].apply(lambda x: x.diff().max()).reset_index()
        # select bad rows
        mask = tmp['report_month'].dt.days > missed_th
        # get mpi of those bad rows
        long_pause_patid = tmp[mask]['mpi'].to_numpy()
        # drop those mpis
        mask = df['mpi'].isin(long_pause_patid)
        df = df[~mask]
        assert np.all(df.groupby('mpi')['report_month'].count() == at_least_rows), 'should be equal'
    return df
