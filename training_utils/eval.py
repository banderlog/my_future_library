import numpy as np
import pandas as pd
import lightgbm as lgb
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.inspection import permutation_importance
from sklearn.model_selection import StratifiedKFold

from training_utils.training import train_lgb_cpu
from training_utils.plotting import plot_shap


def eval_all_model_stuff(df: pd.DataFrame, *, title='', max_display=None,
                         split_by_mpi=False, random_state=42) -> None:
    """ Get target metrics and SHAP values

        :param df: input
        :param title: plot title
        :param max_display: how many features plot for SHAP
        :param split_by_mpi: split by MPI or not
        :param random_state: random seed
        :return: df
    """
    #Split by MPI
    if split_by_mpi:
        train_mpi, test_mpi = train_test_split(df.groupby('mpi')['record_type'].tail(1), test_size=0.20,
                                               random_state=random_state)
        test = df.query('mpi in @test_mpi.index.get_level_values(0)')
        train = df.query('mpi in @train_mpi.index.get_level_values(0)')

        x_train = train.drop(columns='record_type').values
        y_train = train['record_type'].values

        x_val = test.drop(columns='record_type').values
        y_val = test['record_type'].values
    else:
        # get train data
        x_train, x_val, y_train, y_val = train_test_split(df.drop(columns='record_type'), df['record_type'],
                                                          test_size=0.20, random_state=random_state,
                                                          stratify=df['record_type'])
    # train
    model = train_lgb_cpu(x_train, x_val, y_train.ravel(), y_val.ravel(), early_stopping_rounds=10)
    # predict
    probas = model.predict_proba(x_val)[:, 1]
    precision, recall, pr_thresholds = precision_recall_curve(y_val.ravel(), probas)
    pr_auc = auc(recall, precision)

    fig, axs = plt.subplots(1, 3, figsize=(13, 4))
    # learning curves
    lgb.plot_metric(model, ax=axs[0])
    # ROCAUC
    ax = axs[1]
    false_positive_rate, true_positive_rate, roc_thresholds = roc_curve(y_val, probas)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    ax.plot(false_positive_rate, true_positive_rate, label=f'AUC = {roc_auc:.2f}')
    ax.plot([0, 1], [0, 1], ':')
    ax.legend()
    # AUPRC
    ax = axs[2]
    ax.set_title('AUPRC')
    positive_baseline = y_val.sum() / y_val.shape[0]
    ax.plot(recall, precision, label=f'AUC={pr_auc:.2f}\n({positive_baseline:.2f} positive baseline)')
    ax.fill_between(recall, precision, alpha=0.2)
    ax.legend()
    # title
    fig.suptitle(title, weight='bold')
    plt.show()

    # shap
    if split_by_mpi:
        plot_shap(model, train.drop(columns='record_type'), title=title, max_display=max_display)
    else:
        plot_shap(model, x_train, title=title, max_display=max_display)


def stratified_kfold_permutation_importance(df: pd.DataFrame,
                                            n_splits=5, shuffle=True, n_repeats=5,
                                            random_state=42) -> pd.DataFrame:
    """ Do a K-fold stratified by `record_type` cross validation with permutation importance inside each fold

        For all features that are not in first_only or last_only all rows will be used

        Use stylemap on output: `df_perm.style.applymap(colorify, inverse=True).highlight_max(color='#1a000d')`

        In that case:
          * Green is good -- we shuffled values and our accuracy dropped
          * Red is bad -- we shuffled values and our accuracy increased
          * Yellow is also bad -- we shuffled values and nothing changed

        Examples of usage are in 021-kba

        :param df: input
        :param n_splits: how many folds in CV will be
        :param shuffle: shuffle data before CV (should be yes)
        :param n_repeats: how many times we will run `permutation_importance()` per fold
        :param random_state: random state
        :return: dataframe with results
    """
    # prepare data
    X = df.drop(columns='record_type').values
    Y = df['record_type'].values

    # calculate permutation importance for each fold in CV
    perm_all = []
    skf = StratifiedKFold(n_splits=n_splits, shuffle=shuffle, random_state=random_state)
    for train_idx, test_idx in skf.split(X, Y):
        model = train_lgb_cpu(X[train_idx], X[test_idx], Y[train_idx].ravel(), Y[test_idx].ravel(),
                              early_stopping_rounds=10)

        # for each fold we do few permutations and mean the result
        # memory will run out fast, -1 (8) is too much
        perm_importance = permutation_importance(model, X[test_idx], Y[test_idx],
                                                 scoring='average_precision',
                                                 n_jobs=4, n_repeats=n_repeats, random_state=42)

        combo_perm = perm_importance.importances_mean
        perm_all.append(combo_perm)

    # np.ndarray -> pd.Dataframe, calculate mean() over K-folds
    df_perm = pd.DataFrame(np.array(perm_all).mean(axis=0), index=df.drop(columns='record_type').columns).T
    # sort columns from most important to least important
    df_perm = df_perm[df_perm.T.sum(axis=1, skipna=True).sort_values(ascending=False).index]
    return df_perm


def get_stratified_cv_accuracy(df: pd.DataFrame, target='record_type') -> pd.DataFrame:
    """ Do a 10-fold stratified by `record_type` cross validation and return rocauc/auprc scores

        Examples of usage are in 021-kba

        :param df: longitudinal data to create train/test datasets
        :param target: target variable, Y
        :return: rocauc/auprc scores
    """
    X = df.drop(columns=target).values
    Y = df[target].values

    skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=42)

    results = []
    for train_idx, test_idx in skf.split(X, Y):
        # train
        model = train_lgb_cpu(X[train_idx], X[test_idx], Y[train_idx].ravel(), Y[test_idx].ravel(),
                              early_stopping_rounds=10)
        # calculate target metrics
        probas = model.predict_proba(X[test_idx])[:, 1]
        # ROCAUC
        false_positive_rate, true_positive_rate, roc_thresholds = roc_curve(Y[test_idx], probas)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        # AUPRC (same as average_precision_score)
        precision, recall, pr_thresholds = precision_recall_curve(Y[test_idx], probas)
        pr_auc = auc(recall, precision)
        results.append([roc_auc, pr_auc])

    # aggregate results into single dataframe
    df_out = pd.DataFrame(results, columns=['roc_auc', 'pr_auc'])
    df_out = pd.concat([df_out, pd.DataFrame(df_out.mean(axis=0)).T], ignore_index=True)
    df_out = df_out.set_index(pd.Index(df_out.index.tolist()[:-1] + ['MEAN']))
    return df_out


def colorify(val, inverse=False):
    " for pd.DatafFrame.style.applymap()"
    if inverse:
        color = 'red' if val < 0 else 'green'
    else:
        color = 'red' if val > 0 else 'green'

    if val == 0:
        color = 'yellow'
    return 'color: %s' % color
