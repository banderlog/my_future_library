import lightgbm as lgb


def train_lgb_cpu(train_x, test_x, train_y, test_y,
                  early_stopping_rounds=10, verbosity=0) -> lgb.sklearn.LGBMClassifier:
    """ Train LightGBM binary classifier on CPU

        :param train_x: train data, np.ndarray or pd.DataFrame
        :param test_x: test data, np.ndarray or pd.DataFrame
        :param train_y: train ground truth, np.ndarray or pd.Series
        :param test_y: test ground truth, np.ndarray or pd.Series
        :param early_stopping_rounds: Validation score needs to improve at least every
                                      ``early_stopping_rounds`` round(s) to continue training
        :param verbosity: will print message if ``(epoch_num % verbosity) == 0``
        :return: trained model
    """

    # create model
    param = {'objective': 'binary',
             'metric': 'average_precision',
             'is_unbalance': 'true'}
    model_skl = lgb.sklearn.LGBMClassifier(**param)
    # early stopping and verbosity
    # TODO: shutdown Early stopping messages:
    #   https://github.com/microsoft/LightGBM/blob/54facc4d727812075b0c90e5506e521938953b25/python-package/lightgbm/callback.py
    callbacks = [lgb.early_stopping(early_stopping_rounds, verbose=verbosity), lgb.log_evaluation(period=verbosity)]
    # train
    model_skl.fit(train_x, train_y,
                  eval_set=[(train_x, train_y), (test_x, test_y)],
                  eval_names=['train', 'valid'],
                  eval_metric='average_precision',
                  categorical_feature='auto',  # other values do not work with this API properly
                  callbacks=callbacks)

    return model_skl
