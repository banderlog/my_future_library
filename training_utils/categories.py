import pandas as pd


def reduce_cat_levels(df_cat: pd.DataFrame, feature: str, cats_to_reduce: list, new_cat: str) -> pd.DataFrame:
    """ Reduce amount of categorical feature levels

        :param df_cat: dataset with categorical dtypes features
        :param feature: feature name to work with
        :param cats_to_reduce: categories, that will be reduced to `new_cat`
        :param new_cat: new name for reduced categories
        :return: df
    """
    df_inner = df_cat.copy()
    df_inner[feature] = df_inner[feature].astype(str)
    df_inner[feature] = df_inner[feature].replace(cats_to_reduce, new_cat)
    df_inner[feature] = df_inner[feature].astype("category")
    return df_inner


def reduce_secondary_plan_type(df_cat: pd.DataFrame) -> pd.DataFrame:
    """ Reduce secondary_plan_type feature using E.Goliuk findings 12 -> 5 categories """
    f = 'secondary_plan_type'
    rare_cats_ftrs = ["SUP", "INDEM", "TRI_CHAM", "IHS", "HMO_EPO", "POS"]
    high_risk_ftrs = ["MAASGN", "MCASGN", "MEDICAID"]

    df_cat[f] = reduce_cat_levels(df_cat, f, rare_cats_ftrs, "RARE_CATEGORIES")[f]
    df_cat[f] = reduce_cat_levels(df_cat, f, high_risk_ftrs, "AGR_HIGH_RISK")[f]
    return df_cat


def reduce_cob_ed_priority(df_cat):
    """ Reduce cob_ed_priority feature using E.Goliuk findings 13 -> 4  categories """
    f = "cob_ed_priority"
    for s in ["(", ")", "-", "  ", "  "]:
        df_cat["cob_ed_priority"] = df_cat["cob_ed_priority"].str.replace(s, " ", regex=False)
        df_cat["cob_ed_priority"] = df_cat["cob_ed_priority"].str.strip()
        df_cat["cob_ed_priority"] = df_cat["cob_ed_priority"].apply(lambda x: "unknown" if x == "" else x)
        df_cat["cob_ed_priority"] = df_cat["cob_ed_priority"].astype("category")

    low_risk_ftrs = ['GEP COB New Start', 'GEP MCARE Turning 65', 'GEP COB Turning 65',
                     'GEP COB', 'GEP MCARE', 'GEP MCARE New Start', 'COB New Start',
                     'COB', 'Turning 65 COB', 'GEP COB COB', 'GEP MCARE COB',
                     'GEP COB Turning 65 COB', 'GEP Turning 65 COB', 'GEP MCARE Turning 65 COB',
                     'GEP MCARE COB New Start', 'Turning 65 COB New Start', 'GEP', 'GEP Turning 65']

    high_risk_ftrs = ['GEP Turning 65 New Start', 'Turning 65', 'Turning 65 New Start', 'GEP New Start']
    # other_ftrs = ['uninsured patients', 'New Start']

    df_cat[f] = reduce_cat_levels(df_cat, f, low_risk_ftrs, "AGR_LOW_RISK")[f]
    df_cat[f] = reduce_cat_levels(df_cat, f, high_risk_ftrs, "AGR_HIGH_RISK")[f]
    return df_cat
