import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import shap
import itertools
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

from typing import List, Tuple


def plot_vyshyvanka(df: pd.DataFrame, ax: plt.Axes, annot=False,
                    coef_width=50, line_opacity=0.6, marker_opacity=0.1) -> Tuple[plt.Axes, int, np.ndarray]:
    """
        It will build the flow plot "Vyshyvanka": the transitions between possible states of
        observations on the Y-axis over some time periods on the X-axis.

        Line width correlate with the number of transactions.

        NA will not be plotted as a transaction


        :param df: dataframe where columns are observations and rows are time points
        :param ax: an instance of ``matplotlib.axes.Axes`` to plot on
        :param coef_width: the bigger it, the thicker will be the lines
        :param line_opacity: lines opacity
        :param annot: annotations on nodes
        :param marker opacity: markers opacity

        :return: (``matplotlib.axes.Axes``, number_of_observations_without_na, possible_states)

        **Example:**

        .. code-block:: python

            import random
            import pandas as pd
            import matplotlib.pyplot as plt


            obs_num = 1000
            time_points = 8
            states = [1, 2, 3, 4, 5]

            tmp = []
            for _ in range(time_points):
                tmp.append(random.choices(states, k=obs_num,
                                          weights=[0.05, 0.2 ,0.5, 0.2 ,0.05]))

            df = pd.DataFrame(tmp)


            fig, ax = plt.subplots(figsize=(15, 7))
            fig.patch.set_facecolor('white')

            ax, num, relevant_states = plot_vyshyvanka(df, ax)
            _ = ax.set_yticks(relevant_states)
            _ = ax.set_xlabel('Time points')
            _ = ax.set_ylabel('States')
            _ = ax.set_title(f'State transitions over time (n={num})')


    """
    # now NA friendly
    df_plot = df.copy()
    obs_num = len(df_plot.columns)

    # iterate over time points using sliding window with length=2, stride=1
    for row1, row2 in zip(itertools.islice(df_plot.iterrows(), 0, None, 1),
                          itertools.islice(df_plot.iterrows(), 1, None, 1)):
        # get row idxs (from df.iterrows() output)
        x1 = row1[0]
        x2 = row2[0]
        # get row values
        tmp = pd.concat([row1[1], row2[1]], axis=1).to_numpy()
        # count unique pairs was-now states
        pairs, nums = np.unique(tmp, axis=0, return_counts=True)
        # print(pairs)
        # print(df_plot)
        # get all possible states
        states = np.unique(pairs.flatten())
        # calculate line widths for each pair (state transition)
        # line width correlates with observations amount,
        # where ``obs_num * coef_width`` is the maximum width
        widths = nums / obs_num * coef_width

        # add axis to widths in order to merge it with pairs for comfy iteration
        for row in np.hstack([pairs, np.expand_dims(widths, axis=1)]):
            # no lines for NA, still will be on annotations
            if np.isnan(row).any():
                continue

            y1, y2, w = row
            # plot line
            ax.plot([x1, x2], [y1, y2], linewidth=w, color='red', alpha=line_opacity, solid_capstyle='round')
            # plot markers
            # marker opacity correlates with observations amount
            ax.plot(x1, y1, 'o', markersize=2, color='black', alpha=marker_opacity)
            ax.plot(x2, y2, 'o', markersize=2, color='black', alpha=marker_opacity)

    # ANNOTATIONS
    if annot:
        tmp = df_plot.T.apply(lambda x: x.value_counts()).fillna(-1).astype(int)
        yxs = itertools.product(tmp.index, tmp.columns)
        annots = [(tmp.loc[yx], yx[::-1]) for yx in yxs]
        bbox_props = dict(boxstyle="circle,pad=0.4", fc="white", ec="black", lw=1, alpha=0.8)
        for a in annots:
            text, xy = a
            if text == -1:
                text = ''
            else:
                text = str(text)
            ax.annotate(text, xy, ha='center', va='center', bbox=bbox_props)

    return ax, obs_num, states


def plot_individual_shap_by_mpi(*, mpi: int,
                                shap_values: shap.Explanation,
                                cat_dict: dict,
                                model_output: pd.DataFrame) -> plt.Figure:
    """ Wraper for `plot_individual_shap_by_index()`
        Will find index using provided MPI

        :param mpi: patient identifier
        :param shap_values: output of `shap.TreeExplainer()`
        :param cat_dict: output of `data.encode_categorical()`
        :return: patched SHAP waterfall plot
    """
    idx = model_output.query(f'mpi == {mpi}').index[0]
    fig = plot_individual_shap_by_index(patient_index=idx, shap_values=shap_values, cat_dict=cat_dict)
    _ = fig.suptitle(f'Feature importancies for MPI {mpi}', weight='bold')
    return fig


def plot_individual_shap_by_index(*, patient_index: int,
                                  shap_values: shap.Explanation,
                                  cat_dict: dict) -> plt.Figure:
    """ Plot individual SHAP plot using precalculated shap_values

        :param patient_index: index from dataframe
        :param shap_values: output of `shap.TreeExplainer()`
        :param cat_dict: output of `data.encode_categorical()`
        :return: patched SHAP waterfall plot
    """
    # by some reason, previous figure is not closed
    plt.clf()
    # without `show=False` no Figure will be returned
    fig = shap.plots.waterfall(shap_values[patient_index], show=False)
    # white background and title
    fig.patch.set_facecolor('white')
    _ = fig.suptitle('')

    # returned figure has 3 axes
    # 0 -- main, 1 and 2 only for base/predicted risk xticks
    ax = fig.get_axes()[0]
    # label axes
    _ = ax.set_xlabel('log(odds) of predicted risk')
    _ = ax.set_ylabel('Feature values')

    #### change feature values to something human-friendly
    # y labels doubled originally, for different colors we do not need it
    ylabels = ax.get_yticklabels()[:10]
    new_ylabels = []
    for i in ylabels:
        # get value and feature name from plot's ylabels
        val, col = i.get_text().split('=')
        val = float(val.strip())
        col = col.strip()
        orig = "_".join(col.split('_')[:-1])
        # get original human-frendly value
        if not np.isnan(val):
            cat_val = str(cat_dict[orig][int(val)])
        else:
            # somethimes it could be just nan
            cat_val = str(val)
        # create new labels
        new_ylabels.append(" = ".join([cat_val, col]))
    # remove 1/2 of yticks
    _ = ax.set_yticks(ax.get_yticks()[:10])
    # set new labels
    _ = ax.set_yticklabels(new_ylabels, color='black')

    # hide baseline log(odds) at the bottom
    _ = fig.get_axes()[1].set_xticklabels(['', ""])

    # convert final log(odds) to risk at the top (invert logit)
    x = float(fig.get_axes()[2].get_xticklabels()[-1].get_text().split('= ')[-1][:-1].replace('−', '-'))
    risk = np.exp(x) / (1 + np.exp(x))
    _ = fig.get_axes()[2].set_xticklabels([f'{risk * 100:.0f}% risk', ''])

    # change figure size and align title
    fig.set_size_inches(12, 8)
    fig.tight_layout()
    _ = fig.suptitle(f'Feature importancies for index {patient_index}', weight='bold')
    return fig


def eval_model(probas: np.ndarray, preds: np.ndarray, y: np.ndarray, conf_matrix=True,
               title='', figsize=(10, 5), additional_ax_num=0, fmt='d'):
    """ Plot ROCAUC, AUPRC, Confusion matrix on a single figure,
        print classification_report

        :param probas: model.predict_proba(test_x)[:, 1]
        :param preds: model.predict(test_x)
        :param y: ground truth
        :param title: title
        :param figsuze: matplotlib's figure size
        :additional_ax_num: number of axes to reserve,
                            so plots for other functions could be pasted there later
        :fmt: seaborn's annotation format for the confusion matrix
        :return: plot
    """
    # ROCAUC
    false_positive_rate, true_positive_rate, roc_thresholds = roc_curve(y, probas)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    # AUPRC (same as average_precision_score)
    precision, recall, pr_thresholds = precision_recall_curve(y, probas)
    pr_auc = auc(recall, precision)

    # plotting part
    fig, axs = plt.subplots(1, 2 + conf_matrix + additional_ax_num, figsize=figsize)
    fig.patch.set_facecolor('white')
    fig.suptitle(title, fontweight='semibold')

    ax = axs[0]
    ax.set_title('ROCAUC')
    ax.plot(false_positive_rate, true_positive_rate, label=f'AUC = {roc_auc:.2f}')
    ax.plot([0, 1], [0, 1], ':')
    ax.legend(loc='lower right')
    ax.set_ylabel('True Positive Rate')
    ax.set_xlabel('False Positive Rate')

    ax = axs[1]
    ax.set_title('AUPRC')
    positive_baseline = y.sum() / y.shape[0]
    ax.plot(recall, precision, label=f'AUC={pr_auc:.2f}\n({positive_baseline:.2f} positive baseline)')
    ax.fill_between(recall, precision, alpha=0.2)
    ax.legend()
    ax.set_xlabel('Recall (TPR)')
    ax.set_ylabel('Precision')

    if conf_matrix:
        ax = axs[2]
        ax.set_title('Confusion matrix (threshold = 0.5)')
        ax = sns.heatmap(confusion_matrix(y, preds),
                         annot=True, annot_kws={"size": 14}, fmt=fmt, cmap='viridis', square=True, cbar=False, ax=ax)
        _ = ax.set(xlabel='Predicted', ylabel='Truth')
        print(classification_report(y, preds))

    fig.patch.set_facecolor('white')
    fig.tight_layout()
    return fig, axs


def plot_corr_matrix(corrmat: pd.DataFrame, figsize=(10, 10), fmt='g',
                     triangle=True, colors: dict = None, **kwargs) -> plt.Figure:
    """ Plot correlation heatmap

        All ``**kwargs`` will be passed to ``sns.heatmap()``

        :param: corrmat: `df.corr()` output
        :param figsize: `plt.Figure`'s' figsize
        :param fmt: sebon annotation format
        :param triangle: if `True`, will crop upper-triangle
        :param colors: ticks colors as `{'col_name': 'color'}`, default is black
        :return: final plot
    """
    mask = None
    # hide part of plot
    if triangle:
        mask = np.zeros_like(corrmat)
        mask[np.triu_indices_from(mask, k=1)] = True

    # plot correlation heatmap
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax = sns.heatmap(corrmat, annot=True, fmt=fmt, cbar=False, vmin=-1, vmax=1, mask=mask, ax=ax, **kwargs)

    # make sure that xlabels always vertical
    xticks = ax.get_xticklabels()
    ax.set_xticklabels(xticks, rotation=90)

    # set colors to labels
    if colors is not None:
        yticks = ax.get_yticklabels()
        for xt in xticks:
            xt.set_color(colors.get(xt.get_text(), 'k'))
        for yt in yticks:
            yt.set_color(colors.get(yt.get_text(), 'k'))

    ax.set_facecolor('grey')
    fig.patch.set_facecolor('white')
    return fig


def plot_corr_subset(df: pd.DataFrame, col_subset: List[str], fmt='g', method='spearman',
                     figsize=(20, 5), colors: dict = None, vertical=True, **kwargs) -> plt.Figure:
    """ Plot correlation heatmap subset  vs all_others

        All ``**kwargs`` will be passed to ``sns.heatmap()``

        :param: df: full dataframe
        :param col_subset: subset of column names we are interested in
        :param fmt: seaborn's heatmap annotation format
        :param method: pandas's correlation method
        :param figsize: `plt.Figure`'s' figsize
        :param colors: ticks colors as `{'col_name': 'color'}`, default is black
        :param vertical: if `True`, show col_subset on Y axis
        :return: final plot
    """
    # still pd.DataFrame
    corrmat = df.corr(method=method)
    # all columns without col_subset
    diff = corrmat.columns.difference(col_subset, sort=False)
    # remove col_subset from Y axis, leave only col_subset on X axis
    out = corrmat[diff].T[col_subset]
    if vertical:
        # swap axes
        out = out.T
    # plot
    fig = plot_corr_matrix(out, triangle=False, figsize=figsize, colors=colors, fmt=fmt, **kwargs)
    return fig


def plot_shap(model, x_train: pd.DataFrame, title='', max_display=None) -> plt.Figure:
    """ Plot Shapley values of feature importancies for Tree model

        :param model: trained tree model
        :param x_train: model's train data
        :param title: plot title
        :max_display: how many features to plot
        :return: plot

    """
    masker = shap.maskers.Independent(x_train, max_samples=1000)
    explainer = shap.TreeExplainer(model, masker)
    shap_values = explainer(x_train.sample(1000, random_state=69))

    fig, ax = plt.subplots(1, 1)
    shap.summary_plot(shap_values, show=False, max_display=max_display)
    fig.patch.set_facecolor('white')
    # just for vspace reservation
    _ = fig.suptitle(title, weight='bold')
    fig.tight_layout()
    # actual title (x position has changed)
    _ = fig.suptitle(title, weight='bold')
    return fig, shap_values
