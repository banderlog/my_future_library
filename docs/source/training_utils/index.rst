.. test documentation master file, created by
   sphinx-quickstart on Thu Aug  8 15:39:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Training utils
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   data
   eval
   training
   plotting
   categories

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
