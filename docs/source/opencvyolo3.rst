OpenCV wrapper for YOLO3
===============================
.. automodule:: opencv_yolo3
   :members:
