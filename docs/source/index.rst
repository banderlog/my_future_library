.. test documentation master file, created by
   sphinx-quickstart on Thu Aug  8 15:39:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   videoutils
   computervision
   kerasutils
   opencvyolo3
   daugman

   training_utils/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
