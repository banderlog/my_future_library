Daugman
===============================
.. From <https://stackoverflow.com/questions/10669099/italicize-text-containing-a-link>:

*It is the mirrored part of* |text|_

.. _text: https://github.com/banderlog/daugman

.. |text| replace:: *this repo*


**Implementation of:**

.. math::

    max_{(r, x_0, y_0)} \left | G_\sigma * \frac{\partial }{\partial r} \oint\limits_{r, x_0, y_0} \frac{I(x,y)}{2\pi r} ds \right |

| :math:`I(x,y)` is the intensity of a pixel at coordinates :math:`(x,y)` in the image of an iris.
| :math:`r` denotes the radius of various circular regions with the center coordinates at :math:`(x_0, y_0)`.
| :math:`\sigma` is the standard deviation of the Gaussian distribution.
| :math:`G_{\sigma}(r)` denotes a Gaussian filter of the scale sigma (:math:`\sigma`).
| :math:`(x_0, y_0)` is assumed centre coordinates of the iris.
| :math:`s` is the contour of the circle given by the parameters :math:`(r, x_0, y_0)`.


**Example:**

.. image:: _static/iris.png

It is 73-85 time faster than https://github.com/Fejcvk/Iris-Comparator/blob/master/daugman.py

**More info could be found in:**

#. `Iris localization using Daugman’s algorithm <https://www.diva-portal.org/smash/get/diva2:831173/FULLTEXT01.pdf>`_
#. `Study of Different IRIS Recognition Methods <http://www.ijctee.org/files/VOLUME2ISSUE1/IJCTEE_0212_14.pdf>`_

.. automodule:: daugman
   :members:
